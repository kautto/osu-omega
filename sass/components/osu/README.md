# sass/components/osu

Place sass components here which style OpenOSU components. These include
components that are distributed only from UCR and do not exist on drupal.org.
OpenOSU Commerce items may also be placed here.

For example, you might have styles for any of these:

* km
* osu_news
* osu_top

But not styles for these:

* entity_reference
* health
* views

Give every component its own file. If multiple components exist for a module,
create a module sub-folder. Omit the OSU prefix.


