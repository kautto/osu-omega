# sass/components/contrib

Place sass components here which style Drupal contrib components.

For example, you might have styles for any of these:

* views
* webform
* commerce

Give every component its own file. If multiple components exist for a module,
create a module sub-folder.


