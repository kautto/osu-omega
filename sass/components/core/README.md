# sass/components/core

Place sass components here which style Drupal core components but not raw
html elements.

For example, you might have styles for any of these:

* block
* node
* node-form
* field

But you wouldn't have styles for any of these which belong in the base folder:

* p
* table
* input
* etc

## Organization

Give every component its own file. If multiple components exist for a module,
create a module sub-folder.

For example, if your theme styles rendered blocks, a _block.scss is sufficient.
However, if it styles both rendered blocks and the block admin page/component,
you should create a "block" subfolder.

