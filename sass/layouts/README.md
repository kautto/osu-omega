# Layouts

This should contain one folder for every layout supported by the theme. For the
purpose of clarity, the layout for the standard page.tpl.php is referred to as
page.
