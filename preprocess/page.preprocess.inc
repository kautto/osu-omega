<?php

/**
 * Implements hook_preprocess_page().
 */
function osu_omega_preprocess_page(&$variables) {
  // You can use preprocess hooks to modify the variables before they are passed
  // to the theme function or template file.
  #dpm($variables['page']['navigation']);
  $variables['sidebar_count'] = 0;
  foreach (array('sidebar_first', 'sidebar_second') as $region) {
    if (count(element_children($variables['page'][$region])) > 0) {
      $variables['page'][$region . '_present'] = 'present';
    }
    else {
      $variables['page'][$region . '_present'] = 'absent';
    }
  }

  /* Limit main menu to first level. */
  foreach ($variables['page']['navigation']['system_main-menu'] as $key => &$element) {
    if ($key{0} != '#') {
      $element['#below'] = array();
    }
  }

  /* Check if logo exists. */
  $path = drupal_get_path('theme', 'osu_omega') . '/images/site-logo.png';
  if (file_exists($path)) {
     $variables['site_name_logo'] = url($path, array('absolute' => TRUE));
  }
}
