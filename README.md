# OSU Omega

OSU Omega is an OSU branded base theme built on HTML5, Omega 4, and Susy 2.

## Compatibility

All major modern browsers are supported two versions back.

## Developers

### Layout & Grid

Since we use osu_theme for panels layouts, no layouts are provided in OSU Omega.
Instead, we have a single standard page layout.

```
$mobile: 480px ;
$tablet: 724px ;
$desktop: 960px ;

6 columns < 724px
12 columns >= 724px
```

### SASS

#### Abstractions

Contains reusable mixins for use throughout your code. Only generics should go here.
A mixin that renders icons specifically for the osu navbar can stay in the osu navbar component.

#### Base

Contains styles for extremely low level HTML elements.
For example, styles for tags can be found here (p, h1, h2, h3, table, etc).
Very basic Drupal elements, like those included in the styleguide module, may also be included.

#### Components

Contains styles for reusable components such as a news widget or a view.
Components *MUST NEVER* be styled based on any context other than available width
and all components should respond to be viable in any width (mobile first).

```
// Never do this.
.sidebar-one .car  {
  color: scarlet;
  background-color: grey;
}
```

Instead, you should try to abstract whatever is specific to the context
and create a modifier class describing those changes.

```
// Better
.car.buckeye {
  color: scarlet;
  background-color: grey;
}
```

#### Layouts

Layouts contain positioning for high level components like pages or panels layouts.
Each one should have its own folder.

#### Variables

