<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['branding']: Items for the branding region.
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see omega_preprocess_page()
 */
?>

<div class="l-page">
  <header id="header" class="l-header" role="banner">

    <!-- OSU Navbar -->
    <nav role="navigation" id="osu_navbar" aria-labelledby="osu_navbar_heading">

      <h2 id="osu_navbar_heading" class="osu-semantic">Ohio State nav bar</h2>
      <a href="#main-content" id="skip" class="osu-semantic">Skip to main content</a>

      <div class="l-container">
        <div class="univ_info">
          <p class="univ_name"><a href="http://www.osu.edu" title="The Ohio State University">The Ohio State University</a></p>
        </div><!-- /univ_info -->
        <div class="univ_links">
          <div class="links">
            <ul>
              <li><a href="http://www.osu.edu/help.php" class="help">Help</a></li>
              <li><a href="http://buckeyelink.osu.edu/" class="buckeyelink" >BuckeyeLink</a></li>
              <li><a href="http://www.osu.edu/map/" class="map">Map</a></li>
              <li><a href="http://www.osu.edu/findpeople.php" class="findpeople">Find People</a></li>
              <li><a href="https://email.osu.edu/" class="webmail">Webmail</a></li>
              <li><a href="http://www.osu.edu/search/" class="search">Search Ohio State</a></li>
            </ul>
          </div><!-- /links -->
        </div><!-- /univ_links -->
      </div><!-- /container -->
    </nav><!-- /osu_navbar -->

    <div class="l-branding">
      <div class="l-container">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <?php if ($site_name_logo): ?>
            <a class="site-name-logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $site_name_logo; ?>"></a>
        <?php else: ?>
          <?php if ($site_name || $site_slogan): ?>
            <?php if ($site_name): ?>
              <a class="site-name" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
            <?php endif; ?>

            <?php if ($site_slogan): ?>
              <h2 class="site-slogan"><?php print $site_slogan; ?></h2>
            <?php endif; ?>
          <?php endif; ?>
        <?php endif; ?>
      </div>
    </div>

    <nav role="navigation" id="navigation" aria-labelledby="osu_navbar_heading">
      <div class="l-container">
        <?php print render($page['navigation']); ?>
      </div>
    </nav>

  </header>

  <div id="content" class="l-main">
    <div class="l-container l-sidebar-first-<?php print $page['sidebar_first_present']; ?> l-sidebar-second-<?php print $page['sidebar_second_present']; ?>">
      <div class="l-content" role="main">
        <?php print render($page['highlighted']); ?>
        <?php print $breadcrumb; ?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <?php print $messages; ?>
        <?php print render($tabs); ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>
        <?php print render($page['content']); ?>
        <?php print $feed_icons; ?>
      </div>

      <?php print render($page['sidebar_first']); ?>
      <?php print render($page['sidebar_second']); ?>
    </div>
  </div>

  <footer id="footer" class="l-footer" role="contentinfo">
    <div class="l-container">
      <?php print render($page['footer_first']); ?>
      <?php print render($page['footer_second']); ?>
      <?php print render($page['footer_third']); ?>
    </div>
    <div class="l-container">
      <?php print render($page['footer_sub']); ?>
    </div>
  </footer>
</div>
